# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
        if n % 2 == 0
                s = n^2- 1
                return s - ((n ÷ 2) - 1) * 2
        else
		s = n^2
                return s - (n ÷ 2) * 2
	end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
	v = Any[]
	for i in 1:(m + 2)
                if i == 1
                	push!(v, m)
                else
                	if i == 2
                        	push!(v, m^3)
                        else
                        	push!(v, impares_consecutivos(m) + 2 * (i - 3))
               		end
                end
        	print( v[i], " ")
	end
end

function mostra_n(n)
	for i in 1:n
		imprime_impares_consecutivos(i)
		print("\n")
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
